# Drupalized Portfolio

## Introduction

Please visit my portfolio Drupal Site through **[the original link without /portfolio/](http://dev-comm5961-hu-portfolio.pantheonsite.io/)** to give it a try.

You may see "Access Denied" in the title bar but it's totally okay since I've given the views full permission.

You can still visit **the original static HTML site** via [the original link](http://dev-comm5961-hu-portfolio.pantheonsite.io/portfolio/) or another link which you can get from the "Hello, I'm Bernard" portal in the drupalized portfolio.

You'll notice a slight difference between the two versions in both apperance and behavior.

FYI, the Google Analytics code, together with the A/B test, is *not* ported to this site yet.