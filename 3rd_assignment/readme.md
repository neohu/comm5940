# 3rd_assignment

Happy Chinese New Year!

## Links for Testing

Due to this assignment is done with the New Age site, it is inconvenience to achieve the goals I set. So here's a brief instruction to test it:

### First Goal: Log In

Please Log in [here](http://dev-comm5940-hu.pantheonsite.io/user/login/) with the following account and password:

+ Account: Bernard
+ Password: Prof.Bernard

It should automatically grant you 5 points.

### Second Goal: Comment

Here's an [article](http://dev-comm5940-hu.pantheonsite.io/content/second-title-2) you can comment with.
It should automatically grant you 10 points.

### Third Goal: Add an Article

Here's the [link](http://dev-comm5940-hu.pantheonsite.io/node/add/article) to add an article.
After you add one, it should automatically grant you 20 points with a message that thanks you for contributing.

### Check your points

Click [here](http://dev-comm5940-hu.pantheonsite.io/user/33/points) to check your points.

Have fun!